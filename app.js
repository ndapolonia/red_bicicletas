var cookieParser        = require('cookie-parser');
var express             = require('express');
var httpError           = require('http-errors');
var logger              = require('morgan');
var mongoose            = require('mongoose');
var path                = require('path');

var indexRouter         = require('./routes/index');
var usersRouter         = require('./routes/users');
var usuariosRouter      = require('./routes/usuarios');
var tokenRouter         = require('./routes/token');
var bicicletasRouter    = require('./routes/bicicletas');
var bicicletasAPIRouter = require('./routes/api/bicicletas');
var usuariosAPIRouter   = require('./routes/api/usuarios')

var app                 = express();
var mongoDB             = 'mongodb://localhost/red_bicicletas';

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/usuarios', usuariosRouter);
app.use('/token', tokenRouter);
app.use('/bicicletas', bicicletasRouter);
app.use('/api/bicicletas', bicicletasAPIRouter);
app.use('/api/usuarios', usuariosAPIRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(httpError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message  = err.message;
  res.locals.error    = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

mongoose.connect(mongoDB, {useNewUrlParser: true, useFindAndModify: false, useCreateIndex: true});
mongoose.Promise = global.Promise;
mongoose.connection.on('error', console.error.bind(console, 'MongoDB connetion error: '));

module.exports = app;