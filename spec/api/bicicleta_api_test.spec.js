var server      = require('../../bin/www');
var Bicicleta   = require('../../models/bicicleta');
var request     = require('request');
var mongoose    = require('mongoose');

var base_url    = 'http://localhost:3000/api/bicicletas';


describe('Bicicleta API', function(){ 
    beforeAll(function(done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true, useFindAndModify: false, useCreateIndex: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('We are connected to test database!');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if (err) { console.log(err); }
            done();
        });
    });

    afterAll(function(done){
        mongoose.disconnect();
        done();
    });

    describe('GET BICICLETAS /', () => {
        it('Status 200', (done) => {
            request.get(base_url, function(error, response, body){
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });

    describe('POST BICICLETAS /create', () => {
        it('Status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici   = '{"code": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54}';
            
            request.post({
                headers : headers,
                url     : base_url + '/create',
                body    : aBici
                }, function(error, response, body){
                    expect(response.statusCode).toBe(200);
                    var bici = JSON.parse(body).bicicleta;
                    expect(bici.color).toBe("rojo");
                    expect(bici.ubicacion[0]).toBe(-34);
                    expect(bici.ubicacion[1]).toBe(-54);
                    done();
             })
        });
    });

    describe('DELETE BICICLETAS /delete', () => {
        it('STATUS 204', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici   = '{"code": 1, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54}';
            
            request.post({
                headers : headers,
                url     : base_url + '/create',
                body    : aBici
                }, function(error, response, body){
                    expect(response.statusCode).toBe(200);
                    var bici = JSON.parse(body).bicicleta;
                    expect(bici.color).toBe("rojo");
                    expect(bici.ubicacion[0]).toBe(-34);
                    expect(bici.ubicacion[1]).toBe(-54);
    
                    request.delete({
                        headers : headers,
                        url     : base_url + '/delete',
                        body    : aBici
                        }, function(error, response, body){
                            expect(response.statusCode).toBe(204);
                            done();
                     })
             });
        });
    });
    
    describe('UPDATE BICICLETAS /update', () => {
        it('STATUS 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici   = '{"code": 1, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54}';
            
            request.post({
                headers : headers,
                url     : base_url + '/create',
                body    : aBici
                }, function(error, response, body){
                    expect(response.statusCode).toBe(200);
                    var bici = JSON.parse(body).bicicleta;
                    expect(bici.color).toBe("rojo");
                    
                    var bBici   = '{"color": "negro", "modelo": "montaña"}';
                    request.put({
                        headers : headers,
                        url     : base_url + '/1/update',
                        body    : bBici
                        }, function(error, response, body){
                            expect(response.statusCode).toBe(204);
                            request.get(base_url + '/1', function(error, response, body){
                                var result = JSON.parse(body);
                                expect(response.statusCode).toBe(200);
                                expect(result.bicicleta.color).toBe("negro");
                                done();
                            });
                     })
             });
        });
    });

});








