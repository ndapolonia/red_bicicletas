var express             = require('express');
var router              = express.Router();
var bicicletaController = require('../../controllers/api/bicicletaControllerAPI');

router.get('/', bicicletaController.bicicleta_list);
router.get('/:code', bicicletaController.bicicleta_findByCode);
router.post('/create', bicicletaController.bicicleta_create);
router.delete('/delete', bicicletaController.bicicleta_delete);
router.put('/:code/update', bicicletaController.bicicleta_update)

module.exports = router;