var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req, res) {
    Bicicleta.allBicis(function (err, result) {
        res.status(200).json({bicicletas: result});
    });
}

exports.bicicleta_findByCode = function(req, res) {
    Bicicleta.findByCode(req.params.code, function(err, result) {
        if (!result) {
            res.status(404).send(`No existe una bicicleta con el id ${req.params.code}`);
        } else {
            res.status(200).json({bicicleta: result});
        }
    });
}

exports.bicicleta_create = function(req, res) {
    var bici = new Bicicleta({code: req.body.code, color: req.body.color, modelo: req.body.modelo, ubicacion: [req.body.lat, req.body.lng]});
    Bicicleta.add(bici, function(err, result) {
        res.status(200).json({bicicleta: result});
    });
}

exports.bicicleta_delete = function(req, res) {
    Bicicleta.removeByCode(req.body.code, function(err, result){
        res.status(204).send();
    });
}

exports.bicicleta_update = function(req, res) {
    Bicicleta.findByCode(req.params.code, function(err, result){
        if (result) {
            if (req.body.color) { result.color = req.body.color; }
            if (req.body.modelo) { result.modelo = req.body.modelo; }
            if (Array.isArray(req.body.ubicacion) && req.body.ubicacion.length == 1) { result.ubicacion  = [req.body.lat, req.body.lng]; }
            
            Bicicleta.updateByCode(result.code, result, function(err, result){
                res.status(204).send();
            });
        } else {
            res.status(404).send(`No existe una bicicleta con el código ${req.params.code}`);
        }
    });
}
